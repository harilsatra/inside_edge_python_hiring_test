"""Main script for generating output.csv."""
import pandas as pd
#import time

def main():
    # add basic program logic here
    #pass
    
    #Read the CSV file
    pitchdata = pd.read_csv('././data/raw/pitchdata.csv')
    
    # Perform grouping/aggregating/filtering for Pitcher team
    pitch_team = pitchdata.groupby(['PitcherTeamId','HitterSide'],as_index = False)['PA','AB','H','TB','BB','SF','HBP'].sum()
    pitch_team = pitch_team[pitch_team.PA >= 25]
    pitch_team.rename(columns = {'PitcherTeamId':'SubjectId'},inplace = True)
    pitch_team['Split'] = ["vs LHH" if i == 'L' else "vs RHH" for i in pitch_team['HitterSide']]
    pitch_team.drop('HitterSide', axis = 1, inplace = True)
    pitch_team['Subject'] = 'PitcherTeamId'
    
    # Perform grouping/aggregating/filtering for Hitter team         
    hit_team = pitchdata.groupby(['HitterTeamId','PitcherSide'],as_index = False)['PA','AB','H','TB','BB','SF','HBP'].sum()
    hit_team = hit_team[hit_team.PA >= 25]
    hit_team.rename(columns = {'HitterTeamId':'SubjectId'},inplace = True)
    hit_team['Split'] = ["vs LHP" if i == 'L' else "vs RHP" for i in hit_team['PitcherSide']]
    hit_team.drop('PitcherSide', axis = 1, inplace = True)
    hit_team['Subject'] = 'HitterTeamId'
    
    # Perform grouping/aggregating/filtering for individual Pitcher       
    pitcher = pitchdata.groupby(['PitcherId','HitterSide'],as_index = False)['PA','AB','H','TB','BB','SF','HBP'].sum()
    pitcher = pitcher[pitcher.PA >= 25]
    pitcher.rename(columns = {'PitcherId':'SubjectId'},inplace = True)
    pitcher['Split'] = ["vs LHH" if i == 'L' else "vs RHH" for i in pitcher['HitterSide']]
    pitcher.drop('HitterSide', axis = 1, inplace = True)
    pitcher['Subject'] = 'PitcherId'
    
    # Perform grouping/aggregating/filtering for individual Hitter
    hitter = pitchdata.groupby(['HitterId','PitcherSide'],as_index = False)['PA','AB','H','TB','BB','SF','HBP'].sum()
    hitter = hitter[hitter.PA >= 25]
    hitter.rename(columns = {'HitterId':'SubjectId'},inplace = True)
    hitter['Split'] = ["vs LHP" if i == 'L' else "vs RHP" for i in hitter['PitcherSide']]
    hitter.drop('PitcherSide', axis = 1, inplace = True)
    hitter['Subject'] = 'HitterId'
    
    # Combine each individual table/dataframe into a single one
    aggregated_data = pd.concat([pitch_team,hitter,hit_team,pitcher],axis = 0)
    
    
    """ Calculate the stats for each subject as follows:
        
        Batting Average(AVG) = Hits(H) / At Bats(AB)
        On Base Percentage(OBP) = (Hits(H) + Base on balls(BB) + Hit by Pitch(HBP)) / (At Bats(AB) + Base on balls(BB) + Hit By Pitches(HBP) + Sacrifice Flies(SF))
        Slugging Percentage(SLG) = Total Bases(TB) / At Bats(AB)
        On Base Plus Slugging(OPS) = On Base Percentage(OBP) + Slugging Percentage(SLG)
    """
    aggregated_data['AVG'] = (aggregated_data.H / aggregated_data.AB).round(3)
    aggregated_data['OBP'] = ((aggregated_data.H + aggregated_data.BB + aggregated_data.HBP) / (aggregated_data.AB + aggregated_data.BB + aggregated_data.HBP + aggregated_data.SF)).round(3)
    aggregated_data['SLG'] = (aggregated_data.TB / aggregated_data.AB).round(3)
    aggregated_data['OPS'] = ((aggregated_data.TB / aggregated_data.AB) + (aggregated_data.H + aggregated_data.BB + aggregated_data.HBP) / (aggregated_data.AB + aggregated_data.BB + aggregated_data.HBP + aggregated_data.SF)).round(3)
    
    # Unpivot the data frame to obtain all the combinations required and trim the required columns  
    id_vars = ['SubjectId','Split','Subject']
    value_vars = ['AVG','OBP','OPS','SLG']
    aggregated_data = pd.melt(aggregated_data,
                            id_vars = id_vars,
                            value_vars = value_vars,
                            var_name = 'Stat',
                            value_name = 'Value')
    

    # Sorting --> reordering the columns --> export to csv
    sort_order = ['SubjectId','Stat','Split','Subject']
    aggregated_data = aggregated_data.sort_values(by = sort_order)
    columns = ['SubjectId','Stat','Split','Subject','Value']
    aggregated_data = aggregated_data[columns]
    aggregated_data.to_csv('./data/processed/output.csv',index = False)

if __name__ == '__main__':
    #start_time = time.time()
    main()
    #print("--- %s seconds ---" % (time.time() - start_time))
